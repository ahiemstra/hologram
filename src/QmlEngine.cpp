/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2018  <copyright holder> <email>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "QmlEngine.h"

#include <memory>
#include <stdexcept>

#include <QStandardPaths>

#include "CommandLineParser.h"
#include "PreviewImageProvider.h"

#include "multimedia/VideoProxy.h"
#include "multimedia/PlaylistModel.h"
#include "multimedia/AudioGroupedProperty.h"
#include "multimedia/AudioStreamsModel.h"
#include "multimedia/SubtitlesModel.h"

class QmlEngine::Private
{
public:
    static QObject* commandLineParser(QQmlEngine* engine, QJSEngine*)
    {
        auto rawParser = parser.get();
        engine->setObjectOwnership(rawParser, QQmlEngine::CppOwnership);
        return rawParser;
    }

    static QObject* videoProxy(QQmlEngine*, QJSEngine*)
    {
        return video;
    }

    static QObject* playlistModel(QQmlEngine*, QJSEngine*)
    {
        return video->playlist();
    }

    static VideoProxy* video;
    static std::shared_ptr<CommandLineParser> parser;
};

std::shared_ptr<CommandLineParser> QmlEngine::Private::parser;
VideoProxy* QmlEngine::Private::video = nullptr;

QmlEngine::QmlEngine(std::shared_ptr<CommandLineParser> parser)
    : QQmlApplicationEngine(), d(new Private)
{
    d->parser = parser;
    d->video = new VideoProxy{*d->parser, this};
}

QmlEngine::~QmlEngine()
{
}

void QmlEngine::initialize()
{
    registerTypes();

    addImageProvider(QStringLiteral("preview"), new PreviewImageProvider());

    auto path = QStandardPaths::locate(QStandardPaths::AppDataLocation, QStringLiteral("qml/main.qml"));
    if(path.isEmpty())
        throw std::runtime_error("Unable to locate main QML file!");

    load(path);
}

void QmlEngine::registerTypes()
{
    const char* uri = "org.kde.hologram";

    qmlRegisterSingletonType<VideoProxy>(uri, 1, 0, "Video", &Private::videoProxy);
    qmlRegisterSingletonType<PlaylistModel>(uri, 1, 0, "Playlist", &Private::playlistModel);
    qmlRegisterSingletonType<CommandLineParser>(uri, 1, 0, "CommandLine", &Private::commandLineParser);

    qmlRegisterUncreatableType<AudioGroupedProperty>(uri, 1, 0, "AudioGroupedProperty", "Can only be accessed through Video");
    qmlRegisterUncreatableType<AudioStreamsModel>(uri, 1, 0, "AudioStreamsModel", "Can only be accessed through Video");
    qmlRegisterUncreatableType<SubtitlesModel>(uri, 1, 0, "SubtitlesModel", "Can only be accessed through Video");
}
