/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2018  <copyright holder> <email>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "VideoProxy.h"

#include <QMediaPlayer>
#include <QMediaService>
#include <QMediaStreamsControl>
#include <QMediaMetaData>
#include <QTime>
#include <QDebug>

// #include "PlaylistProxy.h"

#include "../CommandLineParser.h"

#include "AudioGroupedProperty.h"
#include "PlaylistModel.h"
#include "AudioStreamsModel.h"
#include "SubtitlesModel.h"

class VideoProxy::Private
{
public:
    QMediaPlayer* player = nullptr;

    PlaylistModel* playlist = nullptr;
    AudioGroupedProperty* audioGroupedProperty = nullptr;
    AudioStreamsModel* audioStreams = nullptr;
    SubtitlesModel* subtitles = nullptr;

    bool fullScreen = false;
};

VideoProxy::VideoProxy(const CommandLineParser& parser, QObject* parent)
    : QObject(parent), d(new Private)
{
    d->fullScreen = parser.startFullScreen();

    recreateMediaPlayer();

    d->player->setMuted(parser.startMuted());

    d->audioGroupedProperty = new AudioGroupedProperty{this};
    d->playlist = new PlaylistModel{this, parser};
    d->audioStreams = new AudioStreamsModel{this};
    d->subtitles = new SubtitlesModel{this};
}

VideoProxy::~VideoProxy()
{
}

PlaylistModel* VideoProxy::playlist() const
{
    return d->playlist;
}

QMediaPlayer* VideoProxy::mediaObject() const
{
    return d->player;
}

AudioGroupedProperty * VideoProxy::audio() const
{
    return d->audioGroupedProperty;
}

AudioStreamsModel * VideoProxy::audioStreams() const
{
    return d->audioStreams;
}

SubtitlesModel * VideoProxy::subtitles() const
{
    return d->subtitles;
}

bool VideoProxy::isPlaying() const
{
    return d->player->state() == QMediaPlayer::PlayingState;
}

bool VideoProxy::isPaused() const
{
    return d->player->state() == QMediaPlayer::PausedState;
}

bool VideoProxy::isStopped() const
{
    return d->player->state() == QMediaPlayer::StoppedState;
}

float VideoProxy::progress() const
{
    if(d->player->duration() <= 0)
        return -1.f;

    return d->player->position() / float(d->player->duration());
}

bool VideoProxy::fullScreen() const
{
    return d->fullScreen;
}

QString VideoProxy::currentTime() const
{
    auto time = QTime{0, 0, 0, 0};
    time = time.addMSecs(int(d->player->position()));
    return time.hour() > 0 ? time.toString("hh:mm:ss") : time.toString("mm:ss");
}

QString VideoProxy::totalTime() const
{
    auto time = QTime{0, 0, 0, 0};
    time = time.addMSecs(int(d->player->duration()));
    return time.hour() > 0 ? time.toString("hh:mm:ss") : time.toString("mm:ss");
}

void VideoProxy::setFullScreen(bool full)
{
    if(full == d->fullScreen)
        return;

    d->fullScreen = full;
    emit fullScreenChanged();
}

void VideoProxy::play()
{
    if(!d->player)
        return;

    d->player->play();
    emit startPlayback();
}

void VideoProxy::pause()
{
    if(!d->player)
        return;

    d->player->pause();
}

void VideoProxy::stop()
{
    if(!d->player)
        return;

    d->player->stop();
}

void VideoProxy::seek(float position)
{
    if(!d->player)
        return;

    d->player->setPosition(position * d->player->duration());
}

void VideoProxy::togglePlayback()
{
    if(isPlaying())
    {
        pause();
    }
    else
    {
        play();
    }
}

void VideoProxy::recreateMediaPlayer()
{
    if(d->player)
    {
        disconnect(d->player, &QMediaPlayer::stateChanged, this, &VideoProxy::playingChanged);
        disconnect(d->player, &QMediaPlayer::stateChanged, this, &VideoProxy::pausedChanged);
        disconnect(d->player, &QMediaPlayer::stateChanged, this, &VideoProxy::stoppedChanged);
        disconnect(d->player, &QMediaPlayer::positionChanged, this, &VideoProxy::progressChanged);
        disconnect(d->player, &QMediaPlayer::positionChanged, this, &VideoProxy::currentTimeChanged);
        disconnect(d->player, &QMediaPlayer::durationChanged, this, &VideoProxy::totalTimeChanged);
    }

    auto oldPlayer = d->player;
    auto oldPosition = d->player ? d->player->position() : 0;
    d->player = new QMediaPlayer{this, QMediaPlayer::VideoSurface};

    connect(d->player, &QMediaPlayer::stateChanged, this, &VideoProxy::playingChanged);
    connect(d->player, &QMediaPlayer::stateChanged, this, &VideoProxy::pausedChanged);
    connect(d->player, &QMediaPlayer::stateChanged, this, &VideoProxy::stoppedChanged);
    connect(d->player, &QMediaPlayer::positionChanged, this, &VideoProxy::progressChanged);
    connect(d->player, &QMediaPlayer::positionChanged, this, &VideoProxy::currentTimeChanged);
    connect(d->player, &QMediaPlayer::durationChanged, this, &VideoProxy::totalTimeChanged);

    emit mediaObjectChanged();

    if(oldPlayer)
    {
        if(oldPlayer->state() == QMediaPlayer::PlayingState)
            d->player->play();

        d->player->setPosition(oldPosition);
        d->player->setMuted(oldPlayer->isMuted());
        d->player->setVolume(oldPlayer->volume());

        oldPlayer->deleteLater();
    }
}
