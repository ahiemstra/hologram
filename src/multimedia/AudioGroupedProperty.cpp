/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2018  <copyright holder> <email>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "AudioGroupedProperty.h"

#include <QMediaPlayer>
#include <QAudio>

#include "VideoProxy.h"

AudioGroupedProperty::AudioGroupedProperty(VideoProxy* parent)
    : QObject(parent)
{
    m_parent = parent;
    connect(m_parent, &VideoProxy::mediaObjectChanged, this, &AudioGroupedProperty::mediaObjectChanged);
    mediaObjectChanged();
}

AudioGroupedProperty::~AudioGroupedProperty()
{
}

bool AudioGroupedProperty::muted() const
{
    if(!m_mediaObject)
        return false;

    return m_mediaObject->isMuted();
}

float AudioGroupedProperty::volume() const
{
    if(!m_mediaObject)
        return 0.f;

    auto volume = m_mediaObject->volume() / 100.f;
    volume = QAudio::convertVolume(volume, QAudio::LinearVolumeScale, QAudio::LogarithmicVolumeScale);
    return volume;
}

void AudioGroupedProperty::setMuted(bool muted)
{
    if(m_mediaObject)
        m_mediaObject->setMuted(muted);
}

void AudioGroupedProperty::setVolume(float volume)
{
    if(!m_mediaObject)
        return;

    volume = QAudio::convertVolume(volume, QAudio::LogarithmicVolumeScale, QAudio::LinearVolumeScale);
    m_mediaObject->setVolume(qRound(volume * 100.f));
}

void AudioGroupedProperty::mediaObjectChanged()
{
    if(m_mediaObject)
    {
        disconnect(m_mediaObject, &QMediaPlayer::mutedChanged, this, &AudioGroupedProperty::mutedChanged);
        disconnect(m_mediaObject, &QMediaPlayer::volumeChanged, this, &AudioGroupedProperty::volumeChanged);
    }

    m_mediaObject = m_parent->mediaObject();
    if(m_mediaObject)
    {
        connect(m_mediaObject, &QMediaPlayer::mutedChanged, this, &AudioGroupedProperty::mutedChanged);
        connect(m_mediaObject, &QMediaPlayer::volumeChanged, this, &AudioGroupedProperty::volumeChanged);
    }

    emit mutedChanged();
    emit volumeChanged();
}
