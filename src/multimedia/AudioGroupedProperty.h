/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2018  <copyright holder> <email>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AUDIOGROUPEDPROPERTY_H
#define AUDIOGROUPEDPROPERTY_H

#include <QObject>

class QMediaPlayer;
class VideoProxy;

/**
 * @todo write docs
 */
class AudioGroupedProperty : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool muted READ muted WRITE setMuted NOTIFY mutedChanged)
    Q_PROPERTY(float volume READ volume WRITE setVolume NOTIFY volumeChanged)

public:
    explicit AudioGroupedProperty(VideoProxy* parent);
    ~AudioGroupedProperty();

    bool muted() const;
    float volume() const;

public Q_SLOTS:
    void setMuted(bool muted);
    void setVolume(float volume);

Q_SIGNALS:
    void mutedChanged();
    void volumeChanged();

private:
    Q_SLOT void mediaObjectChanged();

    VideoProxy* m_parent = nullptr;
    QMediaPlayer* m_mediaObject = nullptr;
};

#endif // AUDIOGROUPEDPROPERTY_H
