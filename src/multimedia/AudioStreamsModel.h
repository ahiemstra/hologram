/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2018  <copyright holder> <email>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AUDIOSTREAMSMODEL_H
#define AUDIOSTREAMSMODEL_H

#include <QAbstractListModel>

class QMediaStreamsControl;
class VideoProxy;

/**
 * @todo write docs
 */
class AudioStreamsModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)

public:
    enum Role
    {
        IndexRole = Qt::UserRole + 1,
        NameRole,
        ActiveRole,
    };

    /**
     * Default constructor
     */
    AudioStreamsModel(VideoProxy* parent);

    /**
     * Destructor
     */
    ~AudioStreamsModel();

    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex & index, int role) const override;

    Q_INVOKABLE void setActive(int index);

    Q_SIGNAL void countChanged();

private:
    struct AudioStream
    {
        int index = -1;
        QString name;
        bool active = false;
    };

    void onMediaObjectChanged();
    void onStreamsChanged();
    void onActiveStreamsChanged();

    int m_activeStream = -1;
    VideoProxy* m_parent = nullptr;
    QMediaStreamsControl* m_streamControl = nullptr;
    std::vector<AudioStream> m_streams;
};

#endif // AUDIOSTREAMSMODEL_H
