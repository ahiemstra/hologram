/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2018  <copyright holder> <email>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PlaylistModel.h"

#include <QDebug>

#include "../CommandLineParser.h"
#include "VideoProxy.h"

PlaylistModel::PlaylistModel(VideoProxy* parent, const CommandLineParser& parser)
    : QAbstractListModel(parent)
{
    m_parent = parent;

    m_playlist = new QMediaPlaylist{this};
    connect(m_playlist, &QMediaPlaylist::mediaChanged, this, &PlaylistModel::itemsChanged);
    connect(m_playlist, &QMediaPlaylist::mediaChanged, this, &PlaylistModel::hasNextChanged);
    connect(m_playlist, &QMediaPlaylist::mediaChanged, this, &PlaylistModel::hasPreviousChanged);
    connect(m_playlist, &QMediaPlaylist::currentIndexChanged, this, &PlaylistModel::currentIndexChanged);
    connect(m_playlist, &QMediaPlaylist::currentIndexChanged, this, &PlaylistModel::hasNextChanged);
    connect(m_playlist, &QMediaPlaylist::currentIndexChanged, this, &PlaylistModel::hasPreviousChanged);
    connect(m_playlist, &QMediaPlaylist::playbackModeChanged, this, &PlaylistModel::playbackModeChanged);

    for(const auto& file : parser.files())
    {
        addItem(file);
    }

    m_parent->mediaObject()->setPlaylist(m_playlist);
    connect(m_parent, &VideoProxy::mediaObjectChanged, [this]() {
        m_parent->mediaObject()->setPlaylist(m_playlist);
    });
}

PlaylistModel::~PlaylistModel()
{
}

QMediaPlaylist* PlaylistModel::mediaPlaylist() const
{
    return m_playlist;
}

bool PlaylistModel::hasNext() const
{
    if(m_playlist->mediaCount() <= 1)
        return false;

    return m_playlist->currentIndex() < m_playlist->mediaCount() - 1;
}

bool PlaylistModel::hasPrevious() const
{
    return m_playlist->currentIndex() > 0;
}

bool PlaylistModel::hasItems() const
{
    return m_playlist->mediaCount() > 0;
}

int PlaylistModel::playbackMode() const
{
    return m_playlist->playbackMode();
}

int PlaylistModel::currentIndex() const
{
    return m_playlist->currentIndex();
}

QString PlaylistModel::currentFileName() const
{
    return m_playlist->currentMedia().canonicalUrl().fileName();
}

int PlaylistModel::addItem(const QUrl& source)
{
    if(m_playlist->addMedia(source))
    {
        emit itemsChanged();
        return m_playlist->mediaCount() - 1;
    }

    return -1;
}

void PlaylistModel::removeItem(int index)
{
    beginRemoveRows(QModelIndex(), index, index);
    m_playlist->removeMedia(index);
    endRemoveRows();
}

QHash<int, QByteArray> PlaylistModel::roleNames() const
{
    static QHash<int, QByteArray> roleNames{
        {FileNameRole, "fileName"},
        {FilePathRole, "filePath"},
        {FileUrlRole, "fileUrl"},
        {PlayingRole, "playing"},
    };
    return roleNames;
}

int PlaylistModel::rowCount(const QModelIndex& parent) const
{
    if(!parent.isValid())
        return m_playlist->mediaCount();

    return 0;
}

QVariant PlaylistModel::data(const QModelIndex& index, int role) const
{
    if(!index.isValid())
        return QVariant();

    switch(role)
    {
        case FileNameRole:
            return m_playlist->media(index.row()).canonicalUrl().fileName();
        case FilePathRole:
            return m_playlist->media(index.row()).canonicalUrl().toLocalFile();
        case FileUrlRole:
            return m_playlist->media(index.row()).canonicalUrl();
        case PlayingRole:
            return m_playlist->currentIndex() == index.row();
        default:
            return QVariant{};
    }
}

void PlaylistModel::setCurrentIndex(int index)
{
    m_playlist->setCurrentIndex(index);
}

void PlaylistModel::setPlaybackMode(int newMode)
{
    m_playlist->setPlaybackMode(static_cast<QMediaPlaylist::PlaybackMode>(newMode));
}

void PlaylistModel::next()
{
    m_playlist->next();
}

void PlaylistModel::previous()
{
    m_playlist->previous();
}
