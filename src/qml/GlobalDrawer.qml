import QtQuick 2.9
import QtQuick.Controls 2.2

import QtGraphicalEffects 1.0

import org.kde.kirigami 2.2 as Kirigami
import org.kde.hologram 1.0 as Hologram

Kirigami.GlobalDrawer {
    id: drawer

    title: "Hologram"
    titleIcon: "mediacontrol"

    width: Kirigami.Units.gridUnit * 15

    drawerOpen: !Kirigami.Settings.isMobile
    modal: Kirigami.Settings.isMobile || !applicationWindow().wideScreen

    signal changePage(string page)

    BlurredBackground {
        parent: drawer.background
        anchors.fill: parent

        backgroundX: -drawer.x
        backgroundY: 0

        Kirigami.Theme.colorSet: Kirigami.Theme.View
    }

    actions: [
        Kirigami.Action {
            text: "Now Playing"
            iconName: "media-playback-start-symbolic"
            enabled: Hologram.Video.playlist.hasItems
            checked: root.page == "now_playing"
            onTriggered: drawer.changePage("now_playing")
        },
        Kirigami.Action {
            text: "Playlist"
            iconName: "source-playlist"
            enabled: Hologram.Video.playlist.hasItems
            checked: root.page == "playlist"
            onTriggered: drawer.changePage("playlist")
        },
        Kirigami.Action {
            text: "All Videos"
            iconName: "folder-videos-symbolic"
            checked: root.page == "videos"
            onTriggered: drawer.changePage("videos")
        },
        Kirigami.Action { separator: true },

        Kirigami.Action {
            text: "Settings"
            iconName: "configure"
            checked: root.page == "settings"
            onTriggered: drawer.changePage("settings")
        },
        Kirigami.Action {
            text: "Exit Fullscreen"
            visible: Hologram.Video.fullScreen
            onTriggered: Hologram.Video.fullScreen = false
        },
        Kirigami.Action {
            text: "Quit"
            iconName: "application-exit"
            onTriggered: Qt.quit()
        }
    ]

    Component.onCompleted: handle.enabled = true;
}
