import QtQuick 2.9
import QtGraphicalEffects 1.0

import org.kde.kirigami 2.2 as Kirigami
import org.kde.hologram 1.0 as Hologram

Item {
    id: background

    clip: true

    property real backgroundX: -(applicationWindow().globalDrawer.x + applicationWindow().globalDrawer.width)
    property real backgroundY: 0
    property color color: Kirigami.Theme.backgroundColor

    GaussianBlur {
        x: background.backgroundX
        y: background.backgroundY
        width: applicationWindow().width
        height: applicationWindow().height
        source: applicationWindow().background
        cached: false
        radius: 10
        visible: !Hologram.Video.stopped
    }

    Rectangle {
        anchors.fill: parent
        color: background.color
        opacity: !Hologram.Video.stopped ? 0.9 : 1
    }
}
