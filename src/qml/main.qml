import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Window 2.0

import QtMultimedia 5.8

import QtGraphicalEffects 1.0

import org.kde.kirigami 2.4 as Kirigami
import org.kde.hologram 1.0 as Hologram

Kirigami.ApplicationWindow {
    id: root

    title: "Hologram"

    property string page: "videos";

    visibility: Hologram.Video.fullScreen ? Window.FullScreen : Window.AutomaticVisibility
    pageStack.initialPage: Qt.resolvedUrl("VideosPage.qml")

    globalDrawer: GlobalDrawer { handleVisible: root.controlsVisible; onChangePage: setPage(page) }
    background: VideoBackground { anchors.fill: parent }

    contextDrawer: Kirigami.Settings.isMobile ? contextDrawer.createObject() : null

    Shortcut {
        sequence: StandardKey.FullScreen
        onActivated: Hologram.Video.fullScreen = !Hologram.Video.fullScreen
    }

    Shortcut {
        sequence: StandardKey.Cancel
        onActivated: Hologram.Video.fullScreen = false
    }

    function setPage(new_page) {
        if(new_page == root.page)
            return;

        root.page = new_page
        switch(new_page) {
            case "now_playing":
                root.pageStack.replace(Qt.resolvedUrl("NowPlayingPage.qml"))
                root.globalDrawer.drawerOpen = false
                return
            case "playlist":
                root.pageStack.replace(Qt.resolvedUrl("PlaylistPage.qml"))
                return
            case "videos":
                root.pageStack.replace(Qt.resolvedUrl("VideosPage.qml"), { sort: "name" })
                return
            case "settings":
                root.pageStack.replace(Qt.resolvedUrl("SettingsPage.qml"))
                return
        }
    }

    Component.onCompleted: {
        if(!Kirigami.Settings.isMobile) {
            root.overlay.modal = null;
        }

        if(Hologram.Playlist.hasItems) {
            Hologram.Video.play()
        }
    }

    Connections {
        target: Hologram.Video

        onStartPlayback: {
            setPage("now_playing")
        }
    }

    Component { id: contextDrawer; Kirigami.ContextDrawer { } }
}
