import QtQuick 2.9
import QtQuick.Controls 2.2

import QtMultimedia 5.8

import org.kde.kirigami 2.0 as Kirigami
import org.kde.hologram 1.0 as Hologram

Item {
    id: base

    Rectangle {
        anchors.fill: parent
        color: "black"

        VideoOutput {
            id: videoOutput
            anchors.fill: parent
            anchors.topMargin: Hologram.Video.fullScreen ? 0 : applicationWindow().pageStack.globalToolBar.height
            source: Hologram.Video
        }
    }
}

