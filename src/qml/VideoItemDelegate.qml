import QtQuick 2.9
import QtQuick.Controls 2.2

import QtGraphicalEffects 1.0

import org.kde.kirigami 2.0 as Kirigami

ItemDelegate {
    id: control

    leftPadding: Kirigami.Units.smallSpacing
    rightPadding: Kirigami.Units.smallSpacing
    topPadding: Kirigami.Units.smallSpacing
    bottomPadding: Kirigami.Units.smallSpacing

    background: Rectangle {
        color: control.hovered ? Kirigami.Theme.highlightColor : "transparent"
        opacity: 0.5;
    }

    property url iconSource

    contentItem: Item {
        Image {
            id: icon

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }
            height: width * 0.75

            source: control.iconSource
            asynchronous: true

            fillMode: Image.PreserveAspectFit

            sourceSize.width: width
            sourceSize.height: height
        }

        Label {
            id: label

            anchors {
                bottom: parent.bottom
                left: parent.left
                right: parent.right
                top: icon.bottom
            }

            text: control.text

            wrapMode: Text.Wrap

            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }
}
