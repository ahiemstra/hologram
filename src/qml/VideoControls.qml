import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.2

import org.kde.kirigami 2.0 as Kirigami

import org.kde.hologram 1.0 as Hologram

Control {
    id: control

    implicitWidth: 500
    implicitHeight: (Kirigami.Settings.isMobile ? Kirigami.Units.iconSizes.medium * 3 : 0) +  Kirigami.Units.iconSizes.medium + Kirigami.Units.smallSpacing * 2;
    Behavior on implicitHeight { NumberAnimation { duration: Kirigami.Units.longDuration } }

    property bool hidden: control.state == "hidden"

    function show() {
        control.state = ""
    }

    function hide() {
        control.state = "hidden"
    }

    function focusPlayButton() {
        control.focus = true;
        progressSlider.handle.focus = true;
    }

    contentItem: RowLayout {
        spacing: Kirigami.Units.smallSpacing

        VideoControlButton {
            id: previousButton

            iconSource: "media-skip-backward"
            enabled: Hologram.Video.playlist.hasPrevious
            onClicked: Hologram.Video.playlist.previous()
        }

        Slider {
            id: progressSlider

            Layout.fillWidth: true

            value: Hologram.Video.progress
            onMoved: Hologram.Video.seek(value)

            handle: Rectangle {
                x: Math.max(progressSlider.visualPosition * progressSlider.availableWidth - progressSlider.availableHeight / 2, 0)
                y: (progressSlider.availableHeight - height) / 2
                implicitWidth: Kirigami.Units.iconSizes.medium
                implicitHeight: Kirigami.Units.iconSizes.medium
                radius: width / 2
                color: handleMouseArea.pressed ? Qt.darker(Kirigami.Theme.highlightColor, 1.5) : Kirigami.Theme.highlightColor

                Kirigami.Icon {
                    anchors.centerIn: parent
                    width: Kirigami.Units.iconSizes.smallMedium;
                    height: width
                    source: Hologram.Video.playing ? "media-playback-pause" : "media-playback-start"
                }

                MouseArea {
                    id: handleMouseArea
                    anchors.fill: parent
                    hoverEnabled: true

                    onClicked: Hologram.Video.playing ? Hologram.Video.pause() : Hologram.Video.play()
                    onPositionChanged: {
                        if(!pressed)
                            return

                        Hologram.Video.seek(parent.x / progressSlider.availableWidth)
                    }

                    drag.target: parent
                    drag.axis: Drag.XAxis
                    drag.minimumX: 0
                    drag.maximumX: progressSlider.availableWidth
                }

                ToolTip.visible: handleMouseArea.containsMouse || handleMouseArea.pressed
                ToolTip.text: "%1 / %2".arg(Hologram.Video.currentTime).arg(Hologram.Video.totalTime)
            }

            background: Rectangle {
                implicitWidth: progressSlider.availableWidth
                implicitHeight: Kirigami.Units.smallSpacing * 2
                radius: height / 2
                color: Kirigami.Theme.buttonBackgroundColor

                Rectangle {
                    width: progressSlider.position * parent.width
                    height: parent.height
                    radius: parent.radius
                    color: Kirigami.Theme.highlightColor
                }
            }
        }

        VideoControlButton {
            id: nextButton

            iconSource: "media-skip-forward"

            flat: true

            enabled: Hologram.Video.playlist.hasNext
            onClicked: Hologram.Video.playlist.next()
        }

        VideoControlButton {
            iconSource: "view-fullscreen"

            checkable: true
            checked: Hologram.Video.fullScreen
            onToggled: Hologram.Video.fullScreen = !Hologram.Video.fullScreen
        }
    }

    Timer {
        id: fullscreenHideTimer
        interval: 5000
        repeat: true
        running: control.opacity > 0 && Hologram.Video.fullScreen && !control.hovered && applicationWindow().page == "now_playing"
        onTriggered: control.hide()
    }

    states: State {
        name: "hidden"
        PropertyChanges { target: control; implicitHeight: 0 }
        PropertyChanges { target: applicationWindow(); controlsVisible: false }
    }
}
