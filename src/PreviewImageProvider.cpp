/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2018  <copyright holder> <email>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PreviewImageProvider.h"

#include <QQuickTextureFactory>
#include <QDebug>
#include <QMimeDatabase>

#include <KIO/PreviewJob>

class PreviewResponse : public QQuickImageResponse
{
public:
    PreviewResponse(const QUrl& path, const QSize& size)
    {
        static QStringList allPlugins = KIO::PreviewJob::availablePlugins();

        m_size = size;

        auto items = KFileItemList();
        items << KFileItem(path);
        m_job = new KIO::PreviewJob{items, size, &allPlugins};
        m_job->setIgnoreMaximumSize(true);
        m_job->start();

        connect(m_job, &KIO::PreviewJob::gotPreview, this, &PreviewResponse::onPreviewReceived);
        connect(m_job, &KIO::PreviewJob::failed, this, &PreviewResponse::onFailed);
    }

    QQuickTextureFactory* textureFactory() const override
    {
        return QQuickTextureFactory::textureFactoryForImage(m_image);
    }

    void cancel() override
    {
        m_job->kill();
    }

private:
    void onPreviewReceived(const KFileItem& item, const QPixmap& pixmap)
    {
        Q_UNUSED(item);

        m_image = pixmap.toImage();

        emit finished();
    }

    void onFailed(const KFileItem& item)
    {
        Q_UNUSED(item);
        emit finished();
    }

    KIO::PreviewJob* m_job = nullptr;
    QImage m_image;
    QSize m_size;
};

PreviewImageProvider::PreviewImageProvider(QObject* parent)
    : QObject(parent), QQuickAsyncImageProvider()
{
}

PreviewImageProvider::~PreviewImageProvider()
{
}

QQuickImageResponse* PreviewImageProvider::requestImageResponse(const QString& id, const QSize& requestedSize)
{
    auto size = requestedSize.isValid() ? requestedSize : QSize{100, 100};
    auto response = new PreviewResponse{QUrl::fromLocalFile(id), size};
    return response;
}
