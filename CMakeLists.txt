project(hologram)

cmake_minimum_required(VERSION 3.4)

find_package(ECM REQUIRED NO_MODULE)
list(APPEND CMAKE_MODULE_PATH ${ECM_MODULE_PATH})
include(KDEInstallDirs)
include(KDECompilerSettings)
include(KDECMakeSettings)
kde_enable_exceptions()

find_package(Qt5 5.10 REQUIRED NO_MODULE COMPONENTS Quick QuickControls2 Widgets Multimedia)

find_package(KF5 5.46 REQUIRED COMPONENTS I18n FileMetaData Baloo IconThemes KIO)

if("${CMAKE_BUILD_TYPE}" MATCHES "[dD][eE][bB][uU][gG]")
    add_definitions(-DQT_QML_DEBUG)
endif()

add_subdirectory(src)

install(FILES org.kde.hologram.desktop DESTINATION ${XDG_APPS_INSTALL_DIR})
install(FILES org.kde.hologram.appdata.xml DESTINATION ${KDE_INSTALL_METAINFODIR})

feature_summary(WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)
